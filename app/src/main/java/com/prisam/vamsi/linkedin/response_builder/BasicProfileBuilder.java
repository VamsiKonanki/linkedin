package com.prisam.vamsi.linkedin.response_builder;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vamsi on 12/26/2016.
 */

public class BasicProfileBuilder {

    /* topCardUrl
     {
       "emailAddress": "konanki.vamsi7@gmail.com",
       "firstName": "VAMSI",
       "formattedName": "VAMSI KONANKI",
       "headline": "Application Developer at YQ Labs",
       "id": "SF1ej8wG-g",
       "industry": "Information Technology and Services",
       "lastName": "KONANKI",
       "location": {
         "country": {"code": "in"},
         "name": "Hyderabad Area, India"
       },
       "pictureUrl":
       "https://media.licdn.com/mpr/mprx/0_-4EOGN8qzH025rukj4KOulTzvWVShTfW44OOk8kzlOW2hlOzOWOYWAoznoFYi_8LjWtOdcWvya4u65Tv4w7Go8eMJa4D65s54w7j3TgN-fDT2PTJNVJPTzP5MUGj75DNlHI-QxzDNny",
       "pictureUrls": {
         "_total": 1,
         "values": [
         "https://media.licdn.com/mpr/mprx/0_Pgtdtg1JwVY0lLXM54mfPWrVHJfphW5JlU7ECiGMJ5Cyh5dnXc7EToipWR_qGIX05N7fGMCpV2ayPZkKB9iQ5QGxS2apPZ4JB9iIrgO049xzPfWKBt1o9MzWfb"]
       },
       "publicProfileUrl": "https://www.linkedin.com/in/vamsi-konanki-12ba41123"
     }
    */


    public class Country {
        @SerializedName("code")
        private String code;

        public String getCode() {
            return this.code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }

    public class Location {

        @SerializedName("country")
        private Country country;

        public Country getCountry() {
            return this.country;
        }

        public void setCountry(Country country) {
            this.country = country;
        }

        @SerializedName("name")
        private String name;

        public String getName() {
            return this.name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public class PictureUrls {

        @SerializedName("_total")
        private int _total;

        public int getTotal() {
            return this._total;
        }

        public void setTotal(int _total) {
            this._total = _total;
        }

        @SerializedName("values")
        private ArrayList<String> values;

        public ArrayList<String> getValues() {
            return this.values;
        }

        public void setValues(ArrayList<String> values) {
            this.values = values;
        }
    }

    @SerializedName("emailAddress")
    private String emailAddress;

    public String getEmailAddress() {
        return this.emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @SerializedName("firstName")
    private String firstName;

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @SerializedName("formattedName")
    private String formattedName;

    public String getFormattedName() {
        return this.formattedName;
    }

    public void setFormattedName(String formattedName) {
        this.formattedName = formattedName;
    }

    @SerializedName("headline")
    private String headline;

    public String getHeadline() {
        return this.headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    @SerializedName("id")
    private String id;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @SerializedName("industry")
    private String industry;

    public String getIndustry() {
        return this.industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    @SerializedName("lastName")
    private String lastName;

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @SerializedName("location")
    private Location location;

    public Location getLocation() {
        return this.location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @SerializedName("pictureUrl")
    private String pictureUrl;

    public String getPictureUrl() {
        return this.pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    @SerializedName("pictureUrls")
    private PictureUrls pictureUrls;

    public PictureUrls getPictureUrls() {
        return this.pictureUrls;
    }

    public void setPictureUrls(PictureUrls pictureUrls) {
        this.pictureUrls = pictureUrls;
    }

    @SerializedName("publicProfileUrl")
    private String publicProfileUrl;

    public String getPublicProfileUrl() {
        return this.publicProfileUrl;
    }

    public void setPublicProfileUrl(String publicProfileUrl) {
        this.publicProfileUrl = publicProfileUrl;
    }

}
