package com.prisam.vamsi.linkedin;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.DeepLinkHelper;
import com.linkedin.platform.LISession;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.errors.LIDeepLinkError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.listeners.DeepLinkListener;
import com.linkedin.platform.utils.Scope;
import com.prisam.vamsi.linkedin.response_builder.BasicProfileBuilder;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ObjectStreamClass;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String PACKAGE = "com.prisam.vamsi.linkedin";

    private static final String host = "api.linkedin.com";
    /* private static final String topCardUrl = "https://"
             + host +
             "/v1/people/~:(first-name,last-name,email-address,formatted-name,phone-numbers,public-profile-url,picture-url,picture-urls::(original))";
 */
    private static final String topCardUrl = "https://" + host + "/v1/people/~:" +
            "(" +
            "id," +
            "headline," +
            "location," +
            "industry," +
            "first-name," +
            "last-name," +
            "email-address," +
            "formatted-name," +
            "phone-numbers," +
            "public-profile-url," +
            "picture-url," +
            "picture-urls::(original)" +
            ")";

    private static final String FETCH_BASIC_INFO = "https://" + host + "/v1/people/~:(id,first-name,last-name,headline,location,industry)";

    private static final String FETCH_CONTACT = "https://" + host + "/v1/people/~:(num-connections,email-address,phone-numbers,main-address)";

    private static final String SHARE_URL = "https://" + host + "/v1/people/~/shares";


    private Button btnLogin, btnShare, btnConnections, btnOpenProfile, btnUserProfile;
    private TextView txt_firstName_value, txt_lastName_value, txt_formattedName_value, txt_emailAddress_value;
    private ImageView imgProfilePicSmall, imgProfilePicLarge;
    private TextView txt_id_value, txt_headline_value, txt_industry_value, txt_location_country_code_value, txt_location_name_value, txt_public_profile_url_value;

  /*  private static final String HOST = "api.linkedin.com";
    private static final String FETCH_BASIC_INFO = "https://" + host + "/v1/people/~:(id,first-name,last-name,headline,location,industry)";
    private static final String FETCH_CONTACT = "https://" + host + "/v1/people/~:(num-connections,email-address,phone-numbers,main-address)";
    private static final String FETCH_PROFILE_PIC = "https://" + host + "/v1/people/~:(picture-urls::(original))";
    private static final String SHARE_URL = "https://" + host + "/v1/people/~/shares";*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLogin = (Button) findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);

        txt_firstName_value = (TextView) findViewById(R.id.txt_firstName_value);
        txt_lastName_value = (TextView) findViewById(R.id.txt_lastName_value);
        txt_formattedName_value = (TextView) findViewById(R.id.txt_formattedName_value);
        txt_emailAddress_value = (TextView) findViewById(R.id.txt_emailAddress_value);

        txt_id_value = (TextView) findViewById(R.id.txt_id_value);
        txt_headline_value = (TextView) findViewById(R.id.txt_headline_value);
        txt_industry_value = (TextView) findViewById(R.id.txt_industry_value);
        txt_location_country_code_value = (TextView) findViewById(R.id.txt_location_country_code_value);
        txt_location_name_value = (TextView) findViewById(R.id.txt_location_name_value);
        txt_public_profile_url_value = (TextView) findViewById(R.id.txt_public_profile_url_value);


        btnShare = (Button) findViewById(R.id.btn_share);
        btnShare.setOnClickListener(this);

        btnConnections = (Button) findViewById(R.id.btn_fetch_connections);
        btnConnections.setOnClickListener(this);

        btnOpenProfile = (Button) findViewById(R.id.btn_open_profile);
        btnOpenProfile.setOnClickListener(this);

        btnUserProfile = (Button) findViewById(R.id.btn_open_user_profile);
        btnUserProfile.setOnClickListener(this);

        imgProfilePicSmall = (ImageView) findViewById(R.id.imageViewSmall);
        imgProfilePicLarge = (ImageView) findViewById(R.id.imageViewLarge);


        generateHashkey();
    }


    // This Method is used to generate "Android Package Name" hash key//yi9JDKxj2qpUNedrvAYD3NGDtMo=
    public void generateHashkey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    PACKAGE,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());

                Log.e(TAG, "Package Name: " + info.packageName);
                Log.e(TAG, "Base 64 Name: " + Base64.encodeToString(md.digest(),
                        Base64.NO_WRAP));


            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(TAG, e.getMessage(), e);
        } catch (NoSuchAlgorithmException e) {
            Log.d(TAG, e.getMessage(), e);
        }
    }

    public void loginLinkedin() {
        LISessionManager.getInstance(
                getApplicationContext()).init(this, buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {

                //Toast.makeText(getApplicationContext(), "success" + LISessionManager.getInstance(getApplicationContext()).getSession().getAccessToken().toString(), Toast.LENGTH_LONG).show();

                btnLogin.setText("Logout");

                APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
                apiHelper.getRequest(MainActivity.this, topCardUrl, new ApiListener() {
//                        apiHelper.getRequest(MainActivity.this, FETCH_BASIC_INFO, new ApiListener() {
//                        apiHelper.getRequest(MainActivity.this, FETCH_CONTACT, new ApiListener() {

                    @Override
                    public void onApiSuccess(ApiResponse s) {
                        //((TextView) findViewById(R.id.response)).setText(s.toString());
                        //((TextView) findViewById(R.id.response)).setText(error.toString());
                                /*Toast.makeText(getApplicationContext(), "Profile fetched successfully " + s.toString(),
                                        Toast.LENGTH_LONG).show();*/

                                /* topCardUrl
                                {
                                  "emailAddress": "konanki.vamsi7@gmail.com",
                                  "firstName": "VAMSI",
                                  "formattedName": "VAMSI KONANKI",
                                  "headline": "Application Developer at YQ Labs",
                                  "id": "SF1ej8wG-g",
                                  "industry": "Information Technology and Services",
                                  "lastName": "KONANKI",
                                  "location": {
                                    "country": {"code": "in"},
                                    "name": "Hyderabad Area, India"
                                  },
                                  "pictureUrl":
                                  "https://media.licdn.com/mpr/mprx/0_-4EOGN8qzH025rukj4KOulTzvWVShTfW44OOk8kzlOW2hlOzOWOYWAoznoFYi_8LjWtOdcWvya4u65Tv4w7Go8eMJa4D65s54w7j3TgN-fDT2PTJNVJPTzP5MUGj75DNlHI-QxzDNny",
                                  "pictureUrls": {
                                    "_total": 1,
                                    "values": [
                                    "https://media.licdn.com/mpr/mprx/0_Pgtdtg1JwVY0lLXM54mfPWrVHJfphW5JlU7ECiGMJ5Cyh5dnXc7EToipWR_qGIX05N7fGMCpV2ayPZkKB9iQ5QGxS2apPZ4JB9iIrgO049xzPfWKBt1o9MzWfb"]
                                  },
                                  "publicProfileUrl": "https://www.linkedin.com/in/vamsi-konanki-12ba41123"
                                }
                                */

                                /*   FETCH_BASIC_INFO
                                * {
                                  "firstName": "VAMSI",
                                  "headline": "Application Developer at YQ Labs",
                                  "id": "SF1ej8wG-g",
                                  "industry": "Information Technology and Services",
                                  "lastName": "KONANKI",
                                  "location": {
                                    "country": {"code": "in"},
                                    "name": "Hyderabad Area, India"
                                  }
                                }*/

                                /*   FETCH_CONTACT
                                {
                                "StatusCode":200,"responseData":"
                                {\n  \"emailAddress\": \"konanki.vamsi7@gmail.com\",
                                \n  \"numConnections\": 500\n}",
                                "Location":""}
                                * */

                        Gson gson = new GsonBuilder().create();
                        JsonParser parser = new JsonParser();
                        JsonElement jsonElement = parser.parse(s.getResponseDataAsString());
                        BasicProfileBuilder target2 = gson.fromJson(jsonElement.getAsJsonObject(), BasicProfileBuilder.class);

                        Log.v(TAG, "Profile json" + s.getResponseDataAsJson());
                        Log.v(TAG, "Profile String" + s.getResponseDataAsString());

                        try {
                            /*Log.v(TAG, "Profile emailAddress : " + s.getResponseDataAsJson().get("emailAddress").toString());
                            Log.v(TAG, "Profile formattedName : " + s.getResponseDataAsJson().get("formattedName").toString());

                            Log.v(TAG, "Profile Headline : " + target2.getHeadline());
                            Log.v(TAG, "Profile Id : " + target2.getId());
                            Log.v(TAG, "Profile Industry : " + target2.getIndustry());
                            Log.v(TAG, "Profile PictureUrl : " + target2.getPictureUrl());
                            Log.v(TAG, "Profile Name : " + target2.getLocation().getName());
                            Log.v(TAG, "Profile Country : " + target2.getLocation().getCountry().getCode());
                            Log.v(TAG, "Profile PictureUrls size : " + target2.getPictureUrls().getValues().size());
                            Log.v(TAG, "Profile PublicProfileUrl : " + target2.getPublicProfileUrl());*/

                            txt_firstName_value.setText(s.getResponseDataAsJson().get("firstName").toString());
                            txt_lastName_value.setText(s.getResponseDataAsJson().get("lastName").toString());
                            txt_formattedName_value.setText(s.getResponseDataAsJson().get("formattedName").toString());
                            txt_emailAddress_value.setText(s.getResponseDataAsJson().get("emailAddress").toString());

                            txt_id_value.setText(target2.getId());
                            txt_headline_value.setText(target2.getHeadline());
                            txt_industry_value.setText(target2.getIndustry());
                            txt_location_country_code_value.setText(target2.getLocation().getCountry().getCode());
                            txt_location_name_value.setText(target2.getLocation().getName());
                            txt_public_profile_url_value.setText(target2.getPublicProfileUrl());

                            Picasso.with(MainActivity.this).load(target2.getPictureUrl())
                                    .into(imgProfilePicSmall);

//                            s.getResponseDataAsJson().getString("pictureUrl")
                            if (target2.getPictureUrls().getValues().size() > 0)
                                Picasso.with(MainActivity.this).load(target2.getPictureUrls().getValues().get(target2.getPictureUrls().getValues().size() - 1))
                                        .into(imgProfilePicLarge);

                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), "Profile Exception " + e.toString(),
                                    Toast.LENGTH_LONG).show();
                        }


                    }

                    @Override
                    public void onApiError(LIApiError error) {
                        //((TextView) findViewById(R.id.response)).setText(error.toString());
                        Toast.makeText(getApplicationContext(), "Profile failed " + error.toString(),
                                Toast.LENGTH_LONG).show();
                    }
                });

            }

            @Override
            public void onAuthError(LIAuthError error) {

                Toast.makeText(getApplicationContext(), "failed " + error.toString(),
                        Toast.LENGTH_LONG).show();
            }
        }, true);

       /* String url = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name)";

        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
        apiHelper.getRequest(this, url, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse apiResponse) {
                // Success!
                Toast.makeText(getApplicationContext(), "failed " + apiResponse.toString(),
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onApiError(LIApiError liApiError) {
                // Error making GET request!

                Toast.makeText(getApplicationContext(), "failed " + liApiError.toString(),
                        Toast.LENGTH_LONG).show();
            }
        });*/
    }


    private static Scope buildScope() {
        return Scope.build(
                Scope.R_BASICPROFILE,
                Scope.R_EMAILADDRESS,
                Scope.W_SHARE,
//                Scope.R_CONTACTINFO,
//                Scope.R_FULLPROFILE,
                Scope.RW_COMPANY_ADMIN
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        LISessionManager.getInstance(getApplicationContext()).onActivityResult(this,
                requestCode, resultCode, data);


        /*Intent intent = new Intent(MainActivity.this, HomePage.class);
        startActivity(intent);*/

        DeepLinkHelper deepLinkHelper = DeepLinkHelper.getInstance();
        deepLinkHelper.onActivityResult(this, requestCode, resultCode, data);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_login:


                if (isLogin()) {
                    logOut();
                } else {
                    loginLinkedin();
                }


                break;
            case R.id.btn_share:
//                shareMessage();
                break;
            case R.id.btn_fetch_connections:
                retrieveConnection();
                break;

            case R.id.btn_open_profile:
                openUserProfile();
                // openOtherUserProfile();
                break;


            case R.id.btn_open_user_profile:
                openOtherUserProfile();
                break;


            default:

        }

    }

    public void shareMessage() {
        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
        apiHelper.postRequest
                (MainActivity.this, SHARE_URL,
                        buildShareMessage("Hello World",
                                "Hello Title",
                                "Hello Descriptions",
                                "http://ankitthakkar90.blogspot.in/",
                                "http://1.bp.blogspot.com/-qffW4zPyThI/VkCSLongZbI/AAAAAAAAC88/oGxWnHRwzBk/s320/10333099_1408666882743423_2079696723_n.png"),
                        new ApiListener() {
                            @Override
                            public void onApiSuccess(ApiResponse apiResponse) {
                                // ((TextView) findViewById(R.id.response)).setText(apiResponse.toString());
                                Toast.makeText(getApplicationContext(), "Share success:  " + apiResponse.toString(),
                                        Toast.LENGTH_LONG).show();
                                Log.e(TAG, "share success" + apiResponse.toString());
                            }

                            @Override
                            public void onApiError(LIApiError error) {
                                //   ((TextView) findViewById(R.id.response)).setText(error.toString());
                                Toast.makeText(getApplicationContext(), "Share failed " + error.toString(),
                                        Toast.LENGTH_LONG).show();
                            }
                        });
    }


    public void retrieveConnection() {
        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
        apiHelper.getRequest(MainActivity.this, "https://api.linkedin.com/v1/people/~/connections?modified=new", new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse response) {
                Log.e(TAG, "share success" + response.toString());
            }

            @Override
            public void onApiError(LIApiError error) {
                // Lconnection errorare success" + error.toString());
                Log.e(TAG, "share success" + error.toString());
            }
        });
    }

    public void openUserProfile() {
        DeepLinkHelper deepLinkHelper = DeepLinkHelper.getInstance();
        deepLinkHelper.openCurrentProfile(MainActivity.this, new DeepLinkListener() {
            @Override
            public void onDeepLinkSuccess() {
                Log.e(TAG, "openUserProfile success");
            }

            @Override
            public void onDeepLinkError(LIDeepLinkError error) {

                Log.e(TAG, "openUserProfile error" + error.toString());
            }
        });
    }

    public void openOtherUserProfile() {
        DeepLinkHelper deepLinkHelper = DeepLinkHelper.getInstance();
        deepLinkHelper.openOtherProfile(MainActivity.this, "72818350", new DeepLinkListener() {
            @Override
            public void onDeepLinkSuccess() {
                Log.e(TAG, "openOtherUserProfile success");
            }

            @Override
            public void onDeepLinkError(LIDeepLinkError error) {
                Log.e(TAG, "openOtherUserProfile error" + error.toString());

            }
        });

    }

    private void logOut() {

        LISessionManager.getInstance(getApplicationContext()).clearSession();
        btnLogin.setText("Login");

        txt_firstName_value.setText("");
        txt_lastName_value.setText("");
        txt_formattedName_value.setText("");
        txt_emailAddress_value.setText("");

        txt_id_value.setText("");
        txt_headline_value.setText("");
        txt_industry_value.setText("");
        txt_location_country_code_value.setText("");
        txt_location_name_value.setText("");
        txt_public_profile_url_value.setText("");


        Picasso.with(MainActivity.this).load(android.R.drawable.screen_background_dark_transparent)
                .into(imgProfilePicLarge);

        Picasso.with(MainActivity.this).load(android.R.drawable.screen_background_dark_transparent)
                .into(imgProfilePicSmall);
    }


    public String buildShareMessage(String comment, String title, String descriptions, String linkUrl, String imageUrl) {
        String shareJsonText = "{ \n" +
                "   \"comment\":\"" + comment + "\"," +
                "   \"visibility\":{ " +
                "      \"code\":\"anyone\"" +
                "   }," +
                "   \"content\":{ " +
                "      \"title\":\"" + title + "\"," +
                "      \"description\":\"" + descriptions + "\"," +
                "      \"submitted-url\":\"" + linkUrl + "\"," +
                "      \"submitted-image-url\":\"" + imageUrl + "\"" +
                "   }" +
                "}";
        return shareJsonText;
    }

    private boolean isLogin() {
        LISessionManager sessionManager = LISessionManager.getInstance(getApplicationContext());
        LISession session = sessionManager.getSession();
        boolean accessTokenValid = session.isValid();
        return accessTokenValid;
    }

    private void RetrieveConnections() {
    /*    final Set<ProfileField> connectionFields = EnumSet.of(ProfileField.ID, ProfileField.MAIN_ADDRESS,
                ProfileField.PHONE_NUMBERS, ProfileField.LOCATION,
                ProfileField.LOCATION_COUNTRY, ProfileField.LOCATION_NAME,
                ProfileField.FIRST_NAME, ProfileField.LAST_NAME, ProfileField.HEADLINE,
                ProfileField.INDUSTRY, ProfileField.CURRENT_STATUS,
                ProfileField.CURRENT_STATUS_TIMESTAMP, ProfileField.API_STANDARD_PROFILE_REQUEST,
                ProfileField.EDUCATIONS, ProfileField.PUBLIC_PROFILE_URL, ProfileField.POSITIONS,
                ProfileField.LOCATION, ProfileField.PICTURE_URL);
        Connections connections = client.getConnectionsForCurrentUser(connectionFields);*/
    }


}